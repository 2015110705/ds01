//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

int main()
{
	int *arr[3];
	int a = 2, b = 3, c = 4;

	arr[0] = &a;
	arr[1] = &b;
	arr[2] = &c;

	printf("포인터의 배열의 배열 요소를 이용한 a, b, c 출력\n");
	printf("a : %d\n", *arr[0]);
	printf("b : %d\n", *arr[1]);
	printf("c : %d\n", *arr[2]);

	return 0;
}