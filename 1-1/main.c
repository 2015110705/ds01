//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

int main()
{
	int ary[3] = { 8, 2, 8 };
	int *p = ary;
	int i;

	//1차원 배열을 이용한 출력
	printf("ary를 이용한 출력\n");
	for (i = 0; i < 3; i++)
		printf("%d ", ary[i]);
	printf("\n\n");

	//포인터를 이용한 출력
	printf("p를 이용한 출력\n");
	for (i = 0; i < 3; i++)
		printf("%d ", *(p + i));
	printf("\n");
}