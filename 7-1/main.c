//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

void selectionSort(int arr[], int size);
int binarySearch(int arr[], int first, int last, int value);
void printArray(int arr[], int size);
void swap(int *x, int *y);
int compare(int x, int y);

int main()
{
	int nLength;
	int nInput;
	int *arr;
	int result;
	int i;

	printf("Enter the number of numbers to generate : ");
	scanf("%d", &nLength);
	arr = (int*)malloc(sizeof(int) * nLength);

	for (i = 0; i < nLength; i++)
		arr[i] = rand() % 999;
	
	//배열 출력
	printArray(arr, nLength);

	//선택정렬
	selectionSort(arr, nLength);

	//배열 출력
	printf("Sorted Array : \n");
	printArray(arr, nLength);

	//검색할 넘버 입력
	printf("Enter the number to search : ");
	scanf("%d", &nInput);

	//이진 검색
	result = binarySearch(arr, 0, nLength - 1, nInput);
	if (result == -1)
		printf("The search number is not present\n");
	else
		printf("The search number is present in list[%d]\n", result);

	return 0;
}

void selectionSort(int arr[], int size)
{
	int i, j;
	int minIndex;

	for (i = 0; i < size; i++)
	{
		minIndex = i;
		for (j = i; j < size; j++)
			if (compare(arr[minIndex], arr[j]) == 1)
				minIndex = j;
		
		swap(&arr[minIndex], &arr[i]);
	}
}

int binarySearch(int arr[], int first, int last, int value)
{
	int mid;

	while (first <= last)
	{
		mid = (first + last) / 2;
		
		if (compare(value, arr[mid]) == 0)
			return mid;
		else if (compare(value, arr[mid]) == -1)
			last = mid - 1;
		else if (compare(value, arr[mid]) == 1)
			first = mid + 1;
	}

	return -1;
}

void printArray(int arr[], int size)
{
	int i;

	for (i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void swap(int *x, int *y)
{
	int temp;

	temp = *x;
	*x = *y;
	*y = temp;
}

int compare(int x, int y)
{
	if (x > y)
		return 1;
	else if (x == y)
		return 0;
	else 
		return -1;
}