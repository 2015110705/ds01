//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

int main()
{
	int ary[2][3] =
	{
		{ 4, 2, 3 },
		{ 5, 2, 3 }
	};
	int *p = &(ary[0][0]);
	int i, j;

	//배열을 이용한 출력
	printf("ary를 이용한 출력\n");
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("%d ", ary[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	//포인터를 이용한 출력
	printf("p를 이용한 출력\n");
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("%d ", *(p + 3 * i + j));
		}
		printf("\n");
	}

	return 0;
}