//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

int sumAry1D_f1(int ary[], int size);
int sumAry1D_f2(int *ary, int size);
int sumAry1D_f3(int ary[6], int size);

int main()
{
	int ary1D[] = { 1, 2, 3, 4, 5, 6 };

	printf("sumAry1D_f1() %d\n",	sumAry1D_f1(ary1D, 6));
	printf("sumAry1D_f2() %d\n",	sumAry1D_f1(ary1D, 6));
	printf("sumAry1D_f3() %d\n",	sumAry1D_f1(ary1D, 6));

	return 0;
}

int sumAry1D_f1(int ary[], int size)
{
	int total = 0;
	int i;

	for (i = 0; i < size; i++)
		total += ary[i];

	return total;
}

int sumAry1D_f2(int *ary, int size)
{
	int total = 0;
	int i;

	for (i = 0; i < size; i++)
		total += *(ary + i);

	return total;
}

int sumAry1D_f3(int ary[6], int size)
{
	int total = 0;
	int i;

	for (i = 0; i < size; i++)
		total += ary[i];

	return total;
}