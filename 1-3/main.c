//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>

int main()
{
	int ary[2][2][3] = 
	{
		{
			{ 1, 2, 3 },
			{ 4, 5, 6 }
		},
		{
			{ 7, 8, 9 },
			{ 10, 11, 12 }
		}
	};
	int *p = &ary[0][0][0];
	int i, j, k;
	
	//배열을 이용한 출력
	printf("ary를 이용한 출력\n");
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 2; j++)
		{
			for (k = 0; k < 3; k++)
			{
				printf("%2d ", ary[i][j][k]);
			}
			printf("\n");
		}
		printf("\n");
	}

	//포인터를 이용한 출력
	printf("p를 이용한 출력\n");
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 2; j++)
		{
			for (k = 0; k < 3; k++)
			{
				printf("%2d ", *(p + 6 * i + 3 * j + k));
			}
			printf("\n");
		}
		printf("\n");
	}

	return 0;
}