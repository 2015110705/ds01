//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

#define SWAP(x, y, t) ((t) = (x), (x) = (y), (y) = (t))
#define COMPARE(x, y) (((x) < (y)) ? -1 : ((x) == (y)) ? 0 : 1)

void selectionSort(int arr[], int size);
int binarySearch(int arr[], int first, int last, int value);
void printArray(int arr[], int size);

int main()
{
	int nLength;
	int nInput;
	int *arr;
	int result;
	int i;

	printf("Enter the number of numbers to generate : ");
	scanf("%d", &nLength);
	arr = (int*)malloc(sizeof(int) * nLength);

	for (i = 0; i < nLength; i++)
		arr[i] = rand() % 999;

	//배열 출력
	printArray(arr, nLength);

	//선택정렬
	selectionSort(arr, nLength);

	//배열 출력
	printf("Sorted Array : \n");
	printArray(arr, nLength);

	//검색할 넘버 입력
	printf("Enter the number to search : ");
	scanf("%d", &nInput);

	//이진 검색
	result = binarySearch(arr, 0, nLength - 1, nInput);
	if (result == -1)
		printf("The search number is not present\n");
	else
		printf("The search number is present in list[%d]\n", result);

	return 0;
}

void selectionSort(int arr[], int size)
{
	int i, j;
	int minIndex;
	int temp;

	for (i = 0; i < size; i++)
	{
		minIndex = i;
		for (j = i; j < size; j++)
			if (COMPARE(arr[minIndex], arr[j]) == 1)
				minIndex = j;

		SWAP(arr[minIndex], arr[i], temp);
	}
}

int binarySearch(int arr[], int first, int last, int value)
{
	int mid = (first + last) / 2;

	if (first > last)
		return -1;

	if (arr[mid] == value)
		return mid;
	else if (arr[mid] > value)
		binarySearch(arr, first, mid - 1, value);
	else if (arr[mid] < value)
		binarySearch(arr, mid + 1, last, value);
	
}

void printArray(int arr[], int size)
{
	int i;

	for (i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}